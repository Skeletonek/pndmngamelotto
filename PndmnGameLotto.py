#!/usr/bin/python

import os
from argparse import ArgumentParser
from colorama import Fore, Back, Style
from sys import argv
from random import randint
from time import sleep
from pandas import read_excel
from vlc import MediaPlayer


delay = 0.05  # Change the delay between showing draw results

argp = ArgumentParser()
argp.add_argument('gamelist')
argp.add_argument('iterations', type=int)


def load_gamelist(args):
    ods_data = read_excel('PndmnGameLotto.ods',
                          engine='odf', sheet_name='Lista gier')
    return ods_data[args.gamelist].dropna().tolist()


def draw():
    sleep(delay)
    generated_number = randint(0, len(gamelist)-1)
    return (gamelist[generated_number].__str__() +
            " (" + (generated_number + 1).__str__() + ")")


args = argp.parse_args()
gamelist = load_gamelist(args)
iterations = args.iterations

music = MediaPlayer("roulette.mp3")
music.audio_set_volume(70)
music.play()
for i in range(1, iterations):
    print(str(i) + ". " + draw())
print("\nA wylosowaną grą przez tą super maszynę losującą jest:\n" +
      Fore.BLACK + Back.GREEN + draw() + Fore.RESET + Back.RESET + "\n\n" +
      "Miłego grania!")
music.stop()
